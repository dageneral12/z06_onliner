from django.forms import ModelForm
from barakholka.models import Category, b_Ads, User_Messages, Sub_Category
#from forum.forms import CustomCaptcha, UserLoginForm, UserRegisterForm, UserCreationForm
from django import forms
from .models import Post, Comments


class MessageForm(forms.ModelFrom):
    class Meta:
        model = User_Messages
        fields = [
            'message_title',
            'message_text'
        ]






class b_Ads_Form(forms.ModelForm):
    class Meta:
        model = b_Ads
        fields = [
            'ad_title',
            'ad_text',
            'ad_type',
            'ad_city',
            'ad_photo',
            'ad_category'
        ]

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comments
        fields = [
            'text'
        ]
#User Registration form

#ThreadForm

#CommentForm

#MessageForm

#SearchForm(?)


