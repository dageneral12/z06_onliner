from django.db import models
from django.urls import reverse
from django.utils.text import slugify

from mptt.models import MPTTModel, TreeForeignKey

class User(models.Model):
    user_id = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    user_name = models.TextField(max_length=30, blank=False, null=False, verbose_name='Enter your user name')
    user_password = models.TextField(max_length=20, blank=False, null=False, verbose_name='Enter your password')
    registered = models.BooleanField(verbose_name='Registered', default=False)
    logged_in = models.BooleanField(verbose_name='Loggedin', default=False)
    registration_date = models.DateTimeField(verbose_name='Registered on', default=0)

    def __str__(self):
        return f'{self.user_name}'

class User_Messages(models.Model):
    user_id = models.ForeignKey('auth.User', on_delete=models.CASCADE, related_name='user_id_id')
    user_name_m = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    message_date = models.DateField(auto_now=True, auto_created=True)
    message_title = models.TextField(verbose_name='Title', blank=False, null=True)
    message_text = models.TextField(verbose_name='Enter your message here')
    #addressed_to = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.message_title}'



class Category(models.Model):
    title = models.CharField(max_length=50)

    def __str__(self):
        return f'{self.title}'

    class Meta:
        verbose_name_plural = 'Categories'


class Sub_Category(models.Model):
    sub_cat_type=models.ForeignKey(Category, on_delete=models.CASCADE)
    sub_category_name = models.CharField(max_length=50, blank=False, null=False, verbose_name='Sub Categories')

    def __str__(self):
        return f'{self.sub_category_name}'

class Cities(models.Model):
    cities_n = models.CharField(max_length=40)

    class Meta:
        verbose_name_plural = 'Cities'

    def __str__(self):
        return f'{self.cities_n}'

class b_Ads(models.Model):
    BUY='Purchase'
    SELL='Sell'
    RENT='Rent'
    ALL='All ads'
    CLOSED='Sold'

    #VITEBSK ='Vitebsk'

    #MINSK = 'Minsk'

    #MOGILEV = 'Mogilev'



    #CITIES = ((VITEBSK, 'Vitebsk'), (MINSK, 'Minsk'), (MOGILEV, 'Mogilev'))

    AD_TYPE_CATEGORIES = ((BUY, 'Purchase'), (SELL, 'Sell'), (RENT, 'Rent'), (ALL, 'All ads'), (CLOSED, 'Sold'))
    ad_category = models.ForeignKey(Sub_Category, on_delete=models.CASCADE)
    goods_price = models.DecimalField(max_digits=10, decimal_places=2)
    ad_title = models.CharField(max_length=32, blank=True, null=True, verbose_name='Ad Title')
    ad_author = models.ForeignKey('auth.User', on_delete=models.CASCADE, blank=True, null=True, verbose_name='Ad Author')
    ad_photo = models.ImageField(verbose_name='Image')
    ad_city = models.ForeignKey(Cities, on_delete=models.CASCADE)
    ad_text = models.TextField(verbose_name='Ad Text', blank=True, null=True)
    ad_type = models.CharField(max_length=32, verbose_name='Ad type', choices=AD_TYPE_CATEGORIES)
    published_date = models.DateField(auto_now=False, auto_now_add=True)
    edited_time = models.DateTimeField(auto_now=True, auto_now_add=False)
    #ad_topic = models.ForeignKey()

    def save(self, *args, **kwargs):
        self.slug = slugify(self.ad_title)
        super(b_Ads, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return self.slug

    def __str__(self):
        return f'{self.ad_title}'



class Comments(models.Model):
    comments_title = models.ForeignKey(b_Ads, on_delete=models.CASCADE)
    comments_author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    ad_comment = models.ForeignKey(b_Ads, on_delete=models.CASCADE, verbose_name='Comments', related_name='ad_comment_id')
    comment_image = models.ImageField(verbose_name='Photo')
    publish_date=models.DateField(auto_now=False, auto_now_add=True)
    comment_text = models.TextField(verbose_name='Comments Field', blank=True, null=True)
    edited_date=models.DateField(auto_now=True, auto_now_add=False)

    def __str__(self):
        return f'{self.comments_title}'












