from django.shortcuts import render
from .models import *
from django.views.generic import ListView
from django.views.generic.detail import SingleObjectMixin
from django.http import HttpResponseRedirect, HttpRequest, HttpResponse
from django.shortcuts import get_object_or_404

def main_page(request):
    pass

def post_comments(request, post_pk):
    if request.method == 'POST':
        form = Comments(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.author = request.user
            comment.published_date = timezone.now ()
            comment.save()
            return redirect('post_detail', pk=post_pk)
    else:
        form = Comments()
    return render(request, 'blog/post_comments.html', {'form': form})

from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from .models import Post, Rate, Comments, Category
from .forms import PostForm, CommentForm
#from django.contrib.auth.decorators import login_required

#@login_required
def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.save()
            return redirect('post_detail', post_pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'blog/post_new.html', {'form': form})

def post_list(request):
    posts = Post.objects.all()
    #count_comments = len(list(Comments.objects.all())
    raitings = Rate.objects.all()
    return render(request, 'blog/post_list.html', {'posts': posts, 'ratings': raitings})

# Create your views here.
def count_comments(request, post_pk):

    sum_comments = int(Comments.objects.filter(post__pk=post_pk).count())
    return sum_comments





def post_detail(request, post_pk):
    #post = Post.objects.get(pk=post_pk)
    post = get_object_or_404(Post, pk=post_pk)
    post.views += 1
    post.sum_comments = count_comments(request, post_pk)
    post.save()

    #post = Post.objects.filter(pk = post_pk).first()
    #feedback = Rate.objects.all().filter(post__pk = post_pk)

    #lst = [rating.rate for rating in ratings]
    #if sum(lst) > 0:
        #result = sum(lst) /len(lst)
    #else:
        #result = 0

    #post.PostCounter += 1
    #post.save()
    comment = Comments.objects.filter(post__pk = post_pk)

    return render(request, 'blog/post_detail.html', {'post': post, 'comment': comment})
                  #{'post': post}, {'comment': comment},
                  #{'count_comments': count_comments})

def post_feedback():
    pass

def post_recommend(request, post_pk):
    posts_by_comments = Post.objects.all().orderby('--sum comments')[0:5]
    posts_by_likes = Post.objects.all().orderby('-like')[0:5]
    posts_by_views = Post.objects.all().orderby('-view')[0:5]


    return render(request, 'blog/recommendations_page.html', {'posts_by_likes': posts_by_likes,
                                                              'posts_by_views': posts_by_views,
                                                              'posts_by_comments': posts_by_comments})


#def post_edit(request, post_pk):
    #post = get_object_or_404(Post, pk=post_pk)
    #if request.method == 'POST':
        #form = PostForm(request.POST, instance=post)
        #if form.is_valid():
            #post=form.save(commit=False)
            #post.author = request.user
            #post.save()
            #return redirect('post_detail', pk=post_pk)
    #else:
        #form = PostForm(instance=post)
    #return render request('blog/post_edit.html', {form: 'form'})

def post_edit(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    if request.method == 'POST':
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            post=form.save(commit=False)
            post.author = request.user
            post.save()
            return redirect('post_detail', post_pk=post.pk)
    else:
        form = PostForm(instance=post)
    return render(request, 'blog/post_edit.html', {'form': form})



def post_comments(request, post_pk):
    if request.method == 'POST':
        form = Comments(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.author = request.user
            comment.published_date = timezone.now ()
            comment.save()
            return redirect('post_detail', pk=post_pk)
    else:
        form = Comments()
    return render(request, 'blog/post_comments.html', {'form': form})

# set up so only the author can edit their posts

#def tag_posts(request, pk):

    #tag=Tags.objects.all().get(title=tag_posts())
    #else:
        #tag.save ()
        #tag.posts.add(Post.objects.get(pk=post_pk))
    #return redirect('post_detail', post__pk=post_pk)
    #else:

        #form1 = TagsForm()
        #form2 = CommentsForm()
        #get_object_or_404(Post, pk=post_pk)
        #post=post.views+=1
        #comments =Comments.objects.filter(post__pk=post_pk)
        #ratings=(Rate.objects.filter(post__pk=post_pk))

        #post.g.comments = len(comments)
        #post.save ()
        #tags = post.tags.all()
    #return render(request, 'blog/post_detail.html',{'form1': form1,' form2': form2, 'post': post, comments': comments', 'tags': tags, 'ratings': ratings)})

def post_confirmation(request, pk):
    pass

def post_draft(request, post_pk):
    pass


def post_delete(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)

    if request.method != "POST":
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.delete()
            post.save()
            return redirect(request, 'blog/post_detail', post_pk=post.pk)
    else:
        form = PostForm(instance=post)
    return render(request, 'blog/post_delete.html', {'form': form})



def post_dislike(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    post.dis += 1
    post.save()
    return render(request, 'blog/post_detail.html', {'post': post})


def post_like(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    post.likes += 1
    post.save()
    return render(request, 'blog/post_detail.html', {'post': post})

def post_dislike(request, post_pk):
    post = get_object_or_404 (Post, pk=post_pk)
    post.dislikes += 1
    post.save()
    return render(request, 'blog/post_detail.html', {'post': post})


#counting comments
# Add post list - post draft




def post_pub(request, post_pk):
    post = get_object_or_404(Post, pk = post_pk)
    post.publish = True
    post.save()



def post_category(request, post_pk):
    posts = Post.objects.all().filter(category__pk = post_pk)
    return render(request, 'blog/post_list.html', {'posts': posts})


#---group by functions---


#only active ads

#Published in 24 hrs

#Group by buy/sell/rent/   etc,

#---Pagination functions---

#Paginate the comments to the original thread

#Paginate the number of threads in a single category

#Present Categories and sub-categories on the main page

#---Manipulate posts----

#Create a post

#Edit a post

#Delete a post

#Mark as post as sold

#---Writing messages

#Selecting a user

#Post a message

#---User Admin

#Register as a user

#Log in as a user

#Delete user profile



#---Search functionality

#Search data 1) on the main page, 2) on the sub-category page 3) in the thread itself

#Send email notification when a message is sent or received


def main(request):
    if request.method == 'POST':
        return render(request)

class User:
    pass

def create_ad():
    pass

def delete_ad():
    pass

def edit_ad():
    pass

def rate_user():
    pass

def delete_account():
    pass


def post_counts():
    pass

def last_24_hrs():
    pass


# Create your views here.
