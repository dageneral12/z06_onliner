
from django.contrib import admin
from .models import User, User_Messages, Category, b_Ads, Comments, Sub_Category, Cities


admin.site.register(Category)
admin.site.register(User)
admin.site.register(User_Messages)
admin.site.register(b_Ads)
admin.site.register(Comments)
admin.site.register(Sub_Category)
admin.site.register(Cities)


