from django.urls import path, include

from barakholka.views import *

urlpatterns = [
    path('', main, name='main')
]
from django.urls import path



#specifies the path to each page, specifies the view function (e.g. post_list), and specifies which html page
#needs to be displayed


urlpatterns = [
    path('post/list/', post_list, name='post_list'),
    path('post/detail/<int:post_pk>/', post_detail, name='post_detail'),
    path('post/edit/<int:post_pk>/', post_edit, name='post_edit'),
    path('post/confirmation/<int:post_pk>/', post_confirmation, name='post_confirmation'),
    path('post/new/', post_new, name='post_new'),
    path('post/delete/<int:post_pk>/', post_delete, name='post_delete'),
    path('post/draft/', post_draft, name='post_draft'),
    path('post/category/<int:post_pk>/', post_category, name='post_category'),
    path('post/like/<int:post_pk>/', post_like, name='post_like'),
    path('post/dislike/<int:post_pk>/', post_dislike, name='post_dislike'),
    path('post/comments/<int:post_pk>/', post_comments, name='post_comments')]
    #path('accounts/login/', views.LoginView.as_view(), name='login')